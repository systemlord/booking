# Schema.org/JSON-LD Hotel Booking Service

This is a demo application for a hotel booking service using schema.org vocabulary and JSON-LD format.

It was developed for the Semantic Web course at University of Innsbruck.
