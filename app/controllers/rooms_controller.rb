class RoomsController < ApplicationController
  @@rooms = [
    {
      id: 'SR',
      name: 'Single Room Deluxe',
      min_persons: 1,
      max_persons: 1,
      price: 210
    },
    {
      id: 'DR',
      name: 'Double Deluxe Room',
      min_persons: 2,
      max_persons: 2,
      price: 300
    },
    {
      id: 'JS',
      name: 'Junior-Suite',
      min_persons: 2,
      max_persons: 4,
      price: 540
    }
  ]

  def index
    @rooms = @@rooms  

    @json = {
			"@context": "http://schema.org",
			"@type": "Hotel",
      "name": "Demo Hotel",
      "address": {
				"@type": "PostalAddress",
				"addressLocality": "Vienna",
				"postalCode": "1070",
				"streetAddress": "Neubausgasse 10/15",
				"addressCountry": "AT"
      },
			"email": "demo-hotel@example.org",
			"url": request.base_url,
      "starRating": {
        "@type": "Rating",
        "ratingValue": 4
      },
			"potentialAction": {
				"@type": "SearchAction",
				"target": {
					"@type": "EntryPoint",
						"urlTemplate": "#{request.base_url}/search?checkinDate={checkinDate}&checkoutDate={checkoutDate}&adults={adults}&children={children}"  ,     
            "httpMethod": "GET",
            "contentType": "application/ld+json"
          },
          "query-input": [
            {
              "@type": "PropertyValueSpecification",
              "valueName": "checkinDate",
              "valueRequired": true,
              "valuePattern": "\d{4}-\d{2}-\d{2}"
            },
            {
              "@type": "PropertyValueSpecification",
              "valueName": "checkoutDate",
              "valueRequired": true,
              "valuePattern": "\d{4}-\d{2}-\d{2}"
            }, 
            { 
              "@type": "PropertyValueSpecification",
              "valueName": "adults",
              "valueRequired": true,
              "minValue": 1,
              "maxValue": 10
            },
	          {
              "@type": "PropertyValueSpecification",
              "valueName": "children",
 	            "valueRequired": false,
              "defaultValue": 0,
	            "minValue": 1,
              "maxValue": 10
            } 
					],
          "result": {
            "@type": "Offer",
            "name-output": "required",
            "itemOffered-output": "required",
            "priceSpecification-output": "required",
            "acceptedPaymentMethod-output": "required",
            "advanceBookingRequirement-output": "required"
          }
				}
			}.to_json
  end

  def search
    adults = params[:adults].to_i
    children = params[:children] || 0
    children = children.to_i
    persons = adults + children
    rooms = @@rooms.select do |r|
      persons >= r[:min_persons] &&
	     persons <= r[:max_persons]
    end
    
    logger.debug "found #{rooms.size} rooms"

		json = []
		

		rooms.each do |room|
			json << {
      	"@context": "http://schema.org/",
				"@type":"Offer",
				"name": room[:name],
				"itemOffered": "#{request.base_url}/rooms/#{room[:id]}/offer/#{Time.now.to_i}",
				"priceSpecification": {
					"@type": "PriceSpecification",
					"price": room[:price],
					"priceCurrency": "EUR",
          "eligibleQuantity": {
            "@type": "QuantitativeValue",
            "value": 1,
            "unitCode": "DAY"
          }
				},
        "acceptedPaymentMethod": "http://purl.org/goodrelations/v1#PaymentMethodCreditCard",
        "advanceBookingRequirement": {
          "@type": "QuantitativeValue",
          "value": 7,
          "unitCode": "DAY"
        },
				"potentialAction": {
					"@type": "BuyAction",
					"actionStatus": "PotentialActionStatus",
					"agent": {
						"@type": "Person",
						"givenName-input": "required",
						"familyName-input": "required",
						"email-input": "required"
					},
          "startTime": params[:checkinDate]+"T00:00:00",
          "endTime": params[:checkoutDate]+"T24:00:00",
          "object": {
            "@type":"Offer",
              "itemOffered": "#{request.base_url}/rooms/#{room[:id]}/offer/#{Time.now.to_i}",
            "priceSpecification": {
              "@type": "PriceSpecification",
              "price": room[:price],
              "priceCurrency": "EUR",
              "eligibleQuantity": {
                "@type": "QuantitativeValue",
                "value": 1,
                "unitCode": "DAY"
              }
            }
          },
					"result": {
						"@type": "Order",
						"orderDate-output": "required",
						"confirmationNumber-output": "required",
						"paymentMethodId-output": "required"
					},
					"target": {
						"@type": "EntryPoint",
						"urlTemplate": "#{request.base_url}/book",
						"httpMethod": "POST",
						"encodingType": "application/ld+json",
						"contentType": "application/ld+json"
					}       
				}
			}	
		end
    render json: json

  end

  def book
    result = {
      "@context": "http://schema.org/",
      "@type": "BuyAction",
      "actionStatus": "CompletedActionStatus",
      "result": {
        "@type": "Order",
        "orderDate": Time.now.utc.iso8601,
        "confirmationNumber": Time.now.to_i.to_s
        #"paymentMethodId": rand.to_s[2..5]
      }
    } 


    render json: result
  end
end
