Rails.application.routes.draw do
 
  root to: 'rooms#index'

  post 'rooms/:id/book', to: 'rooms#book'
  
  get 'search', to: 'rooms#search'

end
